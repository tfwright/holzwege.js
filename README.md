holzwege.js
============

It's a bookmarklet that uses the holzwege.org api to automate importing sources and quotations. Bookmark the following links to install:

* <a href="javascript:(function(){document.body.appendChild(document.createElement('script')).src='https://bitbucket.org/tfwright/holzwege.js/raw/master/js/holzwege.js';})();">holzwege.js</a>
* <a href="javascript:(function(){document.body.appendChild(document.createElement('script')).src='https://bitbucket.org/tfwright/holzwege.js/raw/development/js/holzwege.js';})();">holzwege.js-dev</a>